package com.changer.webstore.domain;

public enum Status {
    CHECKED, UNCHECKED;
}
