package com.changer.webstore.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "purchase")
public class Purchase {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id", updatable = false, nullable = false, unique = true)
  private int id;

  private int numberOfProducts;
  private double sum;
  private Date date;

  @Enumerated(EnumType.STRING)
  private Status status;

  @OneToMany(
      cascade = CascadeType.ALL,
      orphanRemoval = true,
      fetch = FetchType.LAZY,
      mappedBy = "purchase")
  private List<Product> products = new ArrayList<>();

  @ManyToOne
  @JoinColumn(name = "user_id")
  private User user;

  public Purchase() {
  }

  public Purchase(List<Product> products, User user) {
    this.products.addAll(products);
    numberOfProducts = products.size();
    date = new Date();
    for (Product product : products) {
      product.setAmount(product.getAmount() - 1);
    }
    this.user = user;
    sum = 0;
    for (Product product : products) {
      sum += Double.parseDouble(product.getPrice().substring(1));
    }
    status = Status.UNCHECKED;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getNumberOfProducts() {
    return numberOfProducts;
  }

  public void setNumberOfProducts(int numberOfProducts) {
    this.numberOfProducts = numberOfProducts;
  }

  public List<Product> getProducts() {
    return products;
  }

  public void setProducts(List<Product> products) {
    this.products = products;
    this.setNumberOfProducts(products.size());
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public double getSum() {
    return sum;
  }

  public void setSum(double sum) {
    this.sum = sum;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }
}
