package com.changer.webstore.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "product")
public class Product {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id", updatable = false, nullable = false, unique = true)
  private int id;

  private String name;
  private String description;
  private String model;
  private String price;
  private int amount;

  @ManyToMany(
      cascade = {CascadeType.ALL},
      targetEntity = Category.class)
  @JoinTable(
      name = "product_has_category",
      joinColumns = {@JoinColumn(name = "product_id", referencedColumnName = "id")},
      inverseJoinColumns = {@JoinColumn(name = "category_id", referencedColumnName = "id")})
  private List<Category> categories = new ArrayList<>();

  @ManyToMany(
      cascade = {CascadeType.ALL},
      targetEntity = Manufacturer.class)
  @JoinTable(
      name = "product_has_manufacturer",
      joinColumns = {@JoinColumn(name = "product_id", referencedColumnName = "id")},
      inverseJoinColumns = {@JoinColumn(name = "manufacturer_id", referencedColumnName = "id")})
  private List<Manufacturer> manufacturers = new ArrayList<>();

  @ManyToOne
  @JoinColumn(name = "purchase_id")
  private Purchase purchase;

  @ManyToMany(mappedBy = "products")
  private List<User> users = new ArrayList<>();

  public Product() {
  }

  public Product(
      String name,
      String description,
      String model,
      String price,
      int amount,
      List<Category> categories,
      List<Manufacturer> manufacturers) {
    this.name = name;
    this.description = description;
    this.model = model;
    this.price = price;
    this.amount = amount;
    this.categories = categories;
    this.manufacturers = manufacturers;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public int getAmount() {
    return amount;
  }

  public void setAmount(int amount) {
    this.amount = amount;
  }

  public List<Category> getCategories() {
    return categories;
  }

  public void setCategories(List<Category> categories) {
    this.categories = categories;
  }

  public List<Manufacturer> getManufacturers() {
    return manufacturers;
  }

  public void setManufacturers(List<Manufacturer> manufacturers) {
    this.manufacturers = manufacturers;
  }

  public Purchase getPurchase() {
    return purchase;
  }

  public void setPurchase(Purchase purchase) {
    this.purchase = purchase;
  }

  public List<User> getUsers() {
    return users;
  }

  public void setUsers(List<User> users) {
    this.users = users;
  }

}
