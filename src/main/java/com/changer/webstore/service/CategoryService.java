package com.changer.webstore.service;

import com.changer.webstore.domain.Category;
import com.changer.webstore.domain.Product;
import com.changer.webstore.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryService {

    private CategoryRepository categoryRepository;

    @Autowired
    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public CategoryRepository getCategoryRepository() {
        return categoryRepository;
    }

    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public Iterable<Category> getAllCategories() {
        return categoryRepository.findAll();
    }

    public Iterable<Category> getCategoriesById(List<Integer> categoriesIds) {
        return categoryRepository.findCategoriesById(categoriesIds);
    }

    public List<Category> getNoProductCategories(Product product) {
        return ((List<Category>) categoryRepository.findAll())
            .stream()
            .filter(category -> !category.getProducts().contains(product))
            .collect(Collectors.toList());
    }

    @Transactional
    public boolean saveCategory(Category category) {
        if (categoryRepository.findCategoryByName(category.getName()) != null) {
            return false;
        } else {
            categoryRepository.save(category);
            return true;
        }
    }

    @Transactional
    public void updateCategory(Category category) {
        categoryRepository.save(category);
    }

    @Transactional
    public void deleteCategory(Category category) {
        categoryRepository.delete(category);
    }

    public Category getCategory(Category category) {
        return categoryRepository.findCategoryById(category.getId());
    }

    public Category getCategoryByName(String name) {
        return categoryRepository.findCategoryByName(name);
    }

}
