package com.changer.webstore.service;

import com.changer.webstore.domain.Product;
import com.changer.webstore.domain.Purchase;
import com.changer.webstore.domain.Status;
import com.changer.webstore.domain.User;
import com.changer.webstore.repository.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class PurchaseService {

  private PurchaseRepository purchaseRepository;
  private UserService userService;

  @Autowired
  public PurchaseService(PurchaseRepository purchaseRepository, UserService userService) {
    this.purchaseRepository = purchaseRepository;
    this.userService = userService;
  }

  public PurchaseRepository getPurchaseRepository() {
    return purchaseRepository;
  }

  public void setPurchaseRepository(PurchaseRepository purchaseRepository) {
    this.purchaseRepository = purchaseRepository;
  }

  public Iterable<Purchase> getAllPurchases() {
    return purchaseRepository.findAll();
  }

  public Iterable<Purchase> getAllUncheckedPurchases() {
    return purchaseRepository.findPurchasesByStatus(Status.UNCHECKED);
  }

  public Iterable<Purchase> getAllCheckedPurchases() {
    return purchaseRepository.findPurchasesByStatus(Status.CHECKED);
  }

  @Transactional
  public void deletePurchase(Purchase purchase) {
    purchaseRepository.delete(purchase);
  }

  @Transactional
  public void makePurchaseBeingChecked(Purchase purchase) {
    purchaseRepository.findPurchaseById(purchase.getId()).setStatus(Status.CHECKED);
  }

  public void addUserToPurchase(Purchase purchase, User user) {
    purchase.setUser(userService.getUserByLogin(user.getLogin()));
  }

  public boolean initPurchase(List<Product> products, User user) {
    if (products.isEmpty()) {
      return false;
    }
    purchaseRepository.save(new Purchase(products, user));
    return true;
  }
}
