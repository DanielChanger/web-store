package com.changer.webstore.service;

import com.changer.webstore.domain.Product;
import com.changer.webstore.domain.Role;
import com.changer.webstore.domain.User;
import com.changer.webstore.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UserService implements UserDetailsService {
  private UserRepository userRepository;
  private ProductService productService;

  @Autowired
  public UserService(UserRepository userRepository, ProductService productService) {
    this.userRepository = userRepository;
    this.productService = productService;
  }

  @Override
  public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
    return userRepository.findUserByLogin(login);
  }

  public String getCurrentUserName() {
    return SecurityContextHolder.getContext().getAuthentication().getName();
  }

  @Transactional
  public User getCurrentUser() {
    return userRepository.findUserByLogin(getCurrentUserName());
  }

  public Iterable<User> getAllUsers() {
    return userRepository.findAll();
  }

  @Transactional
  public void switchRole(User user, Role role) {
    user.setRole(role);
  }

  public User getUserByLogin(String login) {
    return userRepository.findUserByLogin(login);
  }

  @Transactional
  public void addProductToUser(Product product) {
    User user = getCurrentUser();
    if (user.getProducts().contains(productService.getProduct(product))) {
      return;
    }
    user.addProduct(product);
  }

  @Transactional
  public void deleteProductFromUser(Product product) {
    User user = getCurrentUser();
    user.getProducts().remove(productService.getProduct(product));
  }

  @Transactional
  public void clearUserProducts(User user) {
    user.getProducts().clear();
  }

  public User findUserByEmail(String email) {
    return userRepository.findUserByEmail(email);
  }

  public User findUserByPhoneNum(String phoneNum) {
    return userRepository.findUserByPhoneNum(phoneNum);
  }

  public void saveUser(User user) {
    userRepository.save(user);
  }
}
