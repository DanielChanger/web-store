package com.changer.webstore.service;

import com.changer.webstore.domain.Manufacturer;
import com.changer.webstore.domain.Product;
import com.changer.webstore.repository.ManufacturerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ManufacturerService {
    private ManufacturerRepository manufacturerRepository;

    @Autowired
    public ManufacturerService(ManufacturerRepository manufacturerRepository) {
        this.manufacturerRepository = manufacturerRepository;
    }

    public ManufacturerRepository getManufacturerRepository() {
        return manufacturerRepository;
    }

    public void setManufacturerRepository(ManufacturerRepository manufacturerRepository) {
        this.manufacturerRepository = manufacturerRepository;
    }

    public Iterable<Manufacturer> getAllManufacturers() {
        return manufacturerRepository.findAll();
    }

    public Iterable<Manufacturer> getManufacturersById(List<Integer> manufacturersIds) {
        return manufacturerRepository.findManufacturersById(manufacturersIds);
    }

    public List<Manufacturer> getNoProductManufacturers(Product product) {
        return ((List<Manufacturer>) manufacturerRepository
            .findAll())
            .stream()
            .filter(manufacturer -> !manufacturer.getProducts().contains(product))
            .collect(Collectors.toList());
    }

    @Transactional
    public boolean saveManufacturer(Manufacturer manufacturer) {
        if (manufacturerRepository.findManufacturerByName(manufacturer.getName()) != null) {
            return false;
        } else {
            manufacturerRepository.save(manufacturer);
            return true;
        }
    }

    @Transactional
    public void updateManufacturer(Manufacturer manufacturer) {
        manufacturerRepository.save(manufacturer);
    }

    @Transactional
    public void deleteManufacturer(Manufacturer manufacturer) {
        manufacturerRepository.delete(manufacturer);
    }

    public Manufacturer getManufacturer(Manufacturer manufacturer) {
        return manufacturerRepository.findManufacturerById(manufacturer.getId());
    }
}
