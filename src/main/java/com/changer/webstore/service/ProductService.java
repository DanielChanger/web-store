package com.changer.webstore.service;

import com.changer.webstore.domain.Category;
import com.changer.webstore.domain.Manufacturer;
import com.changer.webstore.domain.Product;
import com.changer.webstore.domain.User;
import com.changer.webstore.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@Service
public class ProductService {

  private ProductRepository productRepository;

  public ProductService() {
  }

  @Autowired
  public ProductService(ProductRepository productRepository) {
    this.productRepository = productRepository;
  }

  public ProductRepository getProductRepository() {
    return productRepository;
  }

  public void setProductRepository(ProductRepository productRepository) {
    this.productRepository = productRepository;
  }

  public Iterable<Product> getAllProducts() {
    return productRepository.findAll();
  }

  public Iterable<Product> getAllProductsWithAtLeastOneItem() {
    return productRepository.findProductsByAmountGreaterThan(0);
  }

  @Transactional
  public void deleteProduct(Product product) {
    productRepository.delete(product);
  }

  @Transactional
  public boolean updateProduct(Product product) {
    return addProduct(product);
  }

  @Transactional
  public boolean addProduct(Product product) {
    try {
      Double.parseDouble(product.getPrice().substring(1));
    } catch (NumberFormatException e) {
      return false;
    }
    productRepository.save(product);
    return true;
  }

  public void addProduct(
      Product product, Iterable<Category> categories, Iterable<Manufacturer> manufacturers) {

    product.setCategories((List<Category>) categories);
    product.setManufacturers((List<Manufacturer>) manufacturers);
    productRepository.save(product);
  }

  public List<Category> getProductCategories(Product product) {
    return product.getCategories();
  }

  public Product getProduct(Product product) {
    return productRepository.findProductById(product.getId());
  }

  @Transactional
  public void deleteProductById(Integer id) {
    productRepository.deleteProductById(id);
  }

  public List<Product> getProductByCategory(Category category) {
    return productRepository.findProductsByCategories(Collections.singletonList(category));
  }

  public List<Product> getProductsByUser(User user) {
    return productRepository.findProductsByUsers(Collections.singletonList(user));
  }
}
