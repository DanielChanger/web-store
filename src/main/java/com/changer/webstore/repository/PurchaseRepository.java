package com.changer.webstore.repository;

import com.changer.webstore.domain.Purchase;
import com.changer.webstore.domain.Status;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PurchaseRepository extends CrudRepository<Purchase, Integer> {

  List<Purchase> findPurchasesByStatus(Status status);

  Purchase findPurchaseById(Integer id);
}
