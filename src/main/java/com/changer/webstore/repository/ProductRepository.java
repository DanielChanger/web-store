package com.changer.webstore.repository;

import com.changer.webstore.domain.Category;
import com.changer.webstore.domain.Product;
import com.changer.webstore.domain.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductRepository extends CrudRepository<Product, Integer> {
  Product findProductById(int id);


  List<Product> findProductsByCategories(List<Category> categories);

  List<Product> findProductsByUsers(List<User> users);

  List<Product> findProductsByAmountGreaterThan(Integer amount);

  void deleteProductById(int id);
}
