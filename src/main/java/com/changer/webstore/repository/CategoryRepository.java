package com.changer.webstore.repository;

import com.changer.webstore.domain.Category;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CategoryRepository extends CrudRepository<Category, Integer> {

  void deleteCategoryByName(String name);

  Category findCategoryById(int id);

  Iterable<Category> findCategoriesById(List<Integer> id);

  Category findCategoryByName(String name);

  Category findCategoryById(Integer id);

}
