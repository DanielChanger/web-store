package com.changer.webstore.repository;

import com.changer.webstore.domain.Manufacturer;
import com.changer.webstore.domain.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ManufacturerRepository extends CrudRepository<Manufacturer, Integer> {

  Manufacturer findManufacturerByName(String name);

  Iterable<Manufacturer> findManufacturersById(List<Integer> id);

  List<Manufacturer> findManufacturersByProducts(Product product);

  Manufacturer findManufacturerById(Integer id);

  void deleteManufacturerByName(String name);
}
