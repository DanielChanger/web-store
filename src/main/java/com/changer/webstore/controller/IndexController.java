package com.changer.webstore.controller;

import com.changer.webstore.service.CategoryService;
import com.changer.webstore.service.ProductService;
import com.changer.webstore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
public class IndexController {

  private UserService userService;
  private CategoryService categoryService;
  private ProductService productService;

  @Autowired
  public IndexController(
      UserService userService, CategoryService categoryService, ProductService productService) {
    this.userService = userService;
    this.categoryService = categoryService;
    this.productService = productService;
  }

  @GetMapping( {"/main", ""})
  public ModelAndView showIndex(
      @RequestParam(defaultValue = "all") String category, Map<String, Object> model) {
    model.put("username", userService.getCurrentUserName());
    model.put("categories", categoryService.getAllCategories());
    if (category.equals("all")) {
      model.put("products", productService.getAllProductsWithAtLeastOneItem());
    } else {
      model.put(
          "products",
          productService.getProductByCategory(categoryService.getCategoryByName(category)));
    }
    model.put("category", category);
    return new ModelAndView("/user/main", model);
  }

}
