package com.changer.webstore.controller;

import com.changer.webstore.domain.Purchase;
import com.changer.webstore.service.PurchaseService;
import com.changer.webstore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
public class PurchaseController {

  private PurchaseService purchaseService;
  private UserService userService;

  @Autowired
  public PurchaseController(PurchaseService purchaseService, UserService userService) {
    this.purchaseService = purchaseService;
    this.userService = userService;
  }

  @GetMapping("/admin/purchases")
  public ModelAndView showPurchases(Map<String, Object> model) {
    model.put("username", userService.getCurrentUserName());
    model.put("checkedPurchases", purchaseService.getAllCheckedPurchases());
    model.put("uncheckedPurchases", purchaseService.getAllUncheckedPurchases());
    return new ModelAndView("/admin/purchases/purchases", model);
  }

  @PostMapping("admin/purchases/deletePurchase")
  public ModelAndView deletePurchase(@ModelAttribute Purchase purchase, Map<String, Object> model) {
    purchaseService.deletePurchase(purchase);
    model.put("username", userService.getCurrentUserName());
    model.put("checkedPurchases", purchaseService.getAllCheckedPurchases());
    model.put("uncheckedPurchases", purchaseService.getAllUncheckedPurchases());
    return new ModelAndView("/admin/purchases/purchases", model);
  }

  @PostMapping("admin/purchases/submitPurchase")
  public ModelAndView confirmPurchase(
      @ModelAttribute Purchase purchase, Map<String, Object> model) {
    purchaseService.makePurchaseBeingChecked(purchase);
    model.put("username", userService.getCurrentUserName());
    model.put("checkedPurchases", purchaseService.getAllCheckedPurchases());
    model.put("uncheckedPurchases", purchaseService.getAllUncheckedPurchases());
    return new ModelAndView("/admin/purchases/purchases", model);
  }
}
