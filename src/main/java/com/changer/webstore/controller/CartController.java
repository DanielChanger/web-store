package com.changer.webstore.controller;

import com.changer.webstore.domain.Product;
import com.changer.webstore.service.CategoryService;
import com.changer.webstore.service.ProductService;
import com.changer.webstore.service.PurchaseService;
import com.changer.webstore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
public class CartController {

  private UserService userService;
  private CategoryService categoryService;
  private ProductService productService;
  private PurchaseService purchaseService;

  @Autowired
  public CartController(
      UserService userService,
      CategoryService categoryService,
      ProductService productService,
      PurchaseService purchaseService) {
    this.userService = userService;
    this.categoryService = categoryService;
    this.productService = productService;
    this.purchaseService = purchaseService;
  }

  @PostMapping("/user/toCart")
  public ModelAndView addToCart(
      @ModelAttribute Product product,
      @RequestParam(defaultValue = "all") String category,
      Map<String, Object> model) {
    userService.addProductToUser(product);
    model.put("username", userService.getCurrentUserName());
    model.put("categories", categoryService.getAllCategories());
    if (category.equals("all")) {
      model.put("products", productService.getAllProducts());
    } else {
      model.put(
          "products",
          productService.getProductByCategory(categoryService.getCategoryByName(category)));
    }
    model.put("category", category);
    return new ModelAndView("/user/main", model);
  }

  @GetMapping("/cart")
  public ModelAndView showCart(Map<String, Object> model) {
    model.put("username", userService.getCurrentUserName());
    model.put("products", productService.getProductsByUser(userService.getCurrentUser()));
    return new ModelAndView("/user/cart");
  }

  @PostMapping("/deleteProductFromCart")
  public ModelAndView deleteFromCart(@ModelAttribute Product product, Map<String, Object> model) {
    userService.deleteProductFromUser(product);
    model.put("username", userService.getCurrentUserName());
    model.put("products", productService.getProductsByUser(userService.getCurrentUser()));
    return new ModelAndView("/user/cart", model);
  }

  @PostMapping("/cart/makeOrder")
  public ModelAndView makeOrder(Map<String, Object> model) {
    if (purchaseService.initPurchase(
        userService.getCurrentUser().getProducts(), userService.getCurrentUser())) {
      model.put(
          "username",
          userService.getCurrentUserName() + ", wait for a call, your order is in process!");
    } else {
      model.put(
          "username", userService.getCurrentUserName() + ", add to cart at least one item first!");
    }
    model.put("categories", categoryService.getAllCategories());
    model.put("products", productService.getAllProducts());
    model.put("category", "all");
    return new ModelAndView("/user/main", model);
  }
}
