package com.changer.webstore.controller;

import com.changer.webstore.domain.Product;
import com.changer.webstore.service.CategoryService;
import com.changer.webstore.service.ManufacturerService;
import com.changer.webstore.service.ProductService;
import com.changer.webstore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
public class ProductController {

  private UserService userService;
  private ManufacturerService manufacturerService;
  private ProductService productService;
  private CategoryService categoryService;

  @Autowired
  public ProductController(
      UserService userService,
      ManufacturerService manufacturerService,
      ProductService productService,
      CategoryService categoryService) {
    this.userService = userService;
    this.manufacturerService = manufacturerService;
    this.productService = productService;
    this.categoryService = categoryService;
  }

  @GetMapping("/admin/products")
  public ModelAndView editProducts(Map<String, Object> model) {
    model.put("username", userService.getCurrentUserName());
    model.put("manufacturers", manufacturerService.getAllManufacturers());
    model.put("products", productService.getAllProducts());
    model.put("categories", categoryService.getAllCategories());
    model.put("status", "");
    return new ModelAndView("/admin/products/products", model);
  }

  @GetMapping("/admin/products/addProduct")
  public ModelAndView addProductForm(Map<String, Object> model) {
    model.put("username", userService.getCurrentUserName());
    model.put("manufacturers", manufacturerService.getAllManufacturers());
    model.put("categories", categoryService.getAllCategories());
    model.put("status", "");
    return new ModelAndView("/admin/products/addProduct", model);
  }

  @PostMapping("/admin/products/addProduct")
  public ModelAndView addProduct(@ModelAttribute Product product, Map<String, Object> model) {

    if (productService.addProduct(product)) {

      model.put("status", "Product is added");
    } else {
      model.put("status", "Price is invalid");
    }
    model.put("username", userService.getCurrentUserName());
    model.put("manufacturers", manufacturerService.getAllManufacturers());
    model.put("products", productService.getAllProducts());
    model.put("categories", categoryService.getAllCategories());
    return new ModelAndView("/admin/products/products", model);
  }

  @GetMapping("/admin/products/editProduct")
  public ModelAndView editProduct(@ModelAttribute Product product, Map<String, Object> model) {
    model.put("username", userService.getCurrentUserName());
    model.put("product", productService.getProduct(product));
    model.put("manufacturers", manufacturerService.getNoProductManufacturers(product));
    model.put("categories", categoryService.getNoProductCategories(product));
    return new ModelAndView("/admin/products/editProduct", model);
  }

  @PostMapping("/admin/products/editProduct")
  public ModelAndView editProductPost(@ModelAttribute Product product, Map<String, Object> model) {

    if (productService.updateProduct(product)) {

      model.put("status", "Product is updated");
    } else {
      model.put("status", "Price is invalid");
    }
    model.put("username", userService.getCurrentUserName());
    model.put("products", productService.getAllProducts());
    model.put("manufacturers", manufacturerService.getAllManufacturers());
    model.put("categories", categoryService.getAllCategories());
    model.put("status", "Updating is successful");
    return new ModelAndView("/admin/products/products", model);
  }

  @PostMapping(path = "/admin/products/deleteProduct")
  public ModelAndView deleteProduct(@RequestParam Integer id, Map<String, Object> model) {
    productService.deleteProductById(id);
    model.put("username", userService.getCurrentUserName());
    model.put("products", productService.getAllProducts());
    model.put("manufacturers", manufacturerService.getAllManufacturers());
    model.put("categories", categoryService.getAllCategories());
    model.put("status", "Deleting is successful");
    return new ModelAndView("/admin/products/products", model);
  }
}
