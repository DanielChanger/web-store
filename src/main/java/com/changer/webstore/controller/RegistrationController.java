package com.changer.webstore.controller;

import com.changer.webstore.domain.Role;
import com.changer.webstore.domain.User;
import com.changer.webstore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(path = "/registration")
public class RegistrationController {

  private UserService userService;

  @Autowired
  public RegistrationController(UserService userService) {
    this.userService = userService;
  }

  private String status = "";

  @GetMapping(path = {"/", ""})
  public ModelAndView registering(Map<String, Object> model) {
    model.put("status", "");
    List<User> users = (List<User>) userService.getAllUsers();
    if (users.isEmpty()) {
      model.put("isAdmin", "true");
    } else {
      model.put("isAdmin", "false");
    }
    model.put("username", userService.getCurrentUserName());
    return new ModelAndView("registration", model);
  }

  @PostMapping(path = "/addUser")
  public ModelAndView add(
      @RequestParam() String name,
      @RequestParam() String login,
      @RequestParam() String password,
      @RequestParam() String passwordRepeat,
      @RequestParam() String email,
      @RequestParam() String phoneNum,
      @RequestParam() String isAdmin,
      Map<String, Object> model) {
    if (name.isEmpty()
        || login.isEmpty()
        || password.isEmpty()
        || email.isEmpty()
        || phoneNum.isEmpty()) {
      status = "You need to fill all the gaps!";
      model.put("status", status);
    } else if (!password.equals(passwordRepeat)) {
      status = "Passwords don't match!";
    } else if (userService.getUserByLogin(login) != null) {
      status = "User with login: " + login + " already exists!";
    } else if (userService.findUserByEmail(email) != null) {
      status = "User with e-mail: " + email + " already exists!";
    } else if (userService.findUserByPhoneNum(phoneNum) != null) {
      status = "User with phoneNum: " + phoneNum + " already exists!";
    } else {
      User user = new User(name, login, password, email, phoneNum);
      if (Boolean.parseBoolean(isAdmin)) {
        user.setRole(Role.ADMIN);
      } else {
        user.setRole(Role.USER);
      }
      userService.saveUser(user);
      status = "You've signed up successfully";
      model.put("status", status);
      return new ModelAndView("login", model);
    }
    model.put("status", status);
    model.put("isAdmin", "false");
    model.put("username", userService.getCurrentUserName());
    return new ModelAndView("registration", model);
  }
}
