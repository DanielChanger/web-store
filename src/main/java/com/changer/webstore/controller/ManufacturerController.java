package com.changer.webstore.controller;

import com.changer.webstore.domain.Manufacturer;
import com.changer.webstore.service.ManufacturerService;
import com.changer.webstore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
public class ManufacturerController {

    private UserService userService;
    private ManufacturerService manufacturerService;

    @Autowired
    public ManufacturerController(UserService userService, ManufacturerService manufacturerService) {
        this.userService = userService;
        this.manufacturerService = manufacturerService;
    }

    @GetMapping("/admin/manufacturers")
    public ModelAndView editManufacturers(Map<String, Object> model) {
        model.put("username", userService.getCurrentUserName());
        model.put("manufacturers", manufacturerService.getAllManufacturers());
        model.put("status", "");
        return new ModelAndView("/admin/manufacturers/manufacturers", model);
    }

    @GetMapping("/admin/manufacturers/addManufacturer")
    public ModelAndView addManufacturerShow(
        @ModelAttribute Manufacturer manufacturer, Map<String, Object> model) {
        model.put("username", userService.getCurrentUserName());
        model.put("manufacturers", manufacturerService.getAllManufacturers());
        model.put("status", "");
        return new ModelAndView("/admin/manufacturers/addManufacturer", model);
    }

    @PostMapping("/admin/manufacturers/addManufacturer")
    public ModelAndView addManufacturerPost(
        @ModelAttribute Manufacturer manufacturer, Map<String, Object> model) {
        if (manufacturerService.saveManufacturer(manufacturer)) {
            model.put("status", "Adding is successful");
        } else {
            model.put("status", "Such manufacturer already exists");
        }
        model.put("username", userService.getCurrentUserName());
        model.put("manufacturers", manufacturerService.getAllManufacturers());
        return new ModelAndView("/admin/manufacturers/manufacturers", model);
    }

    @PostMapping("/admin/manufacturers/deleteManufacturer")
    public ModelAndView deleteManufacturer(
        @ModelAttribute Manufacturer manufacturer, Map<String, Object> model) {
        manufacturerService.deleteManufacturer(manufacturer);
        model.put("username", userService.getCurrentUserName());
        model.put("status", "Deletion is successful");
        model.put("manufacturers", manufacturerService.getAllManufacturers());
        return new ModelAndView("/admin/manufacturers/manufacturers", model);
    }

    @GetMapping("/admin/manufacturers/editManufacturer")
    public ModelAndView updateManufacturerShow(
        @ModelAttribute Manufacturer manufacturer, Map<String, Object> model) {
        model.put("username", userService.getCurrentUserName());
        model.put("manufacturer", manufacturerService.getManufacturer(manufacturer));
        return new ModelAndView("/admin/manufacturers/editManufacturer", model);
    }

    @PostMapping("/admin/manufacturers/editManufacturer")
    public ModelAndView updateManufacturerPost(
        @ModelAttribute Manufacturer manufacturer, Map<String, Object> model) {
        manufacturerService.updateManufacturer(manufacturer);
        model.put("username", userService.getCurrentUserName());
        model.put("manufacturers", manufacturerService.getAllManufacturers());
        model.put("status", "Updating is successful");
        return new ModelAndView("/admin/manufacturers/manufacturers", model);
    }
}
