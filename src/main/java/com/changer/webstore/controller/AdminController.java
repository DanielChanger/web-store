package com.changer.webstore.controller;

import com.changer.webstore.domain.Purchase;
import com.changer.webstore.service.PurchaseService;
import com.changer.webstore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/admin")
@PreAuthorize("hasAuthority('ADMIN')")
public class AdminController {

  private final UserService userService;
    private final PurchaseService purchaseService;

  @Autowired
  public AdminController(UserService userService, PurchaseService purchaseService) {
    this.userService = userService;
      this.purchaseService = purchaseService;
  }

  @GetMapping(path = {"/main", "/", ""})
  public ModelAndView getAdmin(Map<String, Object> model) {
    model.put("username", userService.getCurrentUserName());
    model.put("status", "");
      model.put("numOfUncheckedPurchases", ((List<Purchase>) purchaseService.getAllUncheckedPurchases()).size());
    return new ModelAndView("/admin/main", model);
  }
}
