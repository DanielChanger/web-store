package com.changer.webstore.controller;

import com.changer.webstore.domain.Category;
import com.changer.webstore.service.CategoryService;
import com.changer.webstore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
public class CategoryController {

    private UserService userService;
    private CategoryService categoryService;

    @Autowired
    public CategoryController(UserService userService, CategoryService categoryService) {
        this.userService = userService;
        this.categoryService = categoryService;
    }

    @GetMapping("/admin/categories")
    public ModelAndView editCategories(Map<String, Object> model) {
        model.put("username", userService.getCurrentUserName());
        model.put("categories", categoryService.getAllCategories());
        model.put("status", "");
        return new ModelAndView("/admin/categories/categories", model);
    }

    @GetMapping("/admin/categories/addCategory")
    public ModelAndView addCategoryShow(
        @ModelAttribute Category category, Map<String, Object> model) {
        model.put("username", userService.getCurrentUserName());
        model.put("categories", categoryService.getAllCategories());
        model.put("status", "");
        return new ModelAndView("/admin/categories/addCategory", model);
    }

    @PostMapping("/admin/categories/addCategory")
    public ModelAndView addCategoryPost(
        @ModelAttribute Category category, Map<String, Object> model) {
        if (categoryService.saveCategory(category)) {
            model.put("status", "Adding is successful");
        } else {
            model.put("status", "Such category already exists");
        }
        model.put("username", userService.getCurrentUserName());
        model.put("categories", categoryService.getAllCategories());
        return new ModelAndView("/admin/categories/categories", model);
    }

    @PostMapping("/admin/categories/deleteCategory")
    public ModelAndView deleteCategory(@ModelAttribute Category category, Map<String, Object> model) {
        categoryService.deleteCategory(category);
        model.put("username", userService.getCurrentUserName());
        model.put("status", "Deletion is successful");
        model.put("categories", categoryService.getAllCategories());
        return new ModelAndView("/admin/categories/categories", model);
    }

    @GetMapping("/admin/categories/editCategory")
    public ModelAndView updateCategoryShow(
        @ModelAttribute Category category, Map<String, Object> model) {
        model.put("username", userService.getCurrentUserName());
        model.put("category", categoryService.getCategory(category));
        return new ModelAndView("/admin/categories/editCategory", model);
    }

    @PostMapping("/admin/categories/editCategory")
    public ModelAndView updateCategoryPost(
        @ModelAttribute Category category, Map<String, Object> model) {
        categoryService.updateCategory(category);
        model.put("username", userService.getCurrentUserName());
        model.put("categories", categoryService.getAllCategories());
        model.put("status", "Updating is successful");
        return new ModelAndView("/admin/categories/categories", model);
    }
}
