package com.changer.webstore.controller;

import com.changer.webstore.domain.Role;
import com.changer.webstore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/admin/users")
    public ModelAndView editUsers(Map<String, Object> model) {
        model.put("username", userService.getCurrentUserName());
        model.put("users", userService.getAllUsers());
        return new ModelAndView("/admin/users", model);
    }

    @PostMapping("/admin/editUsers")
    public ModelAndView editUsersPost(
        @RequestParam() String login, @RequestParam() Role role, Map<String, Object> model) {
        userService.switchRole(userService.getUserByLogin(login), role);
        model.put("username", userService.getCurrentUserName());
        model.put("users", userService.getAllUsers());
        return new ModelAndView("/admin/users", model);
    }
}
